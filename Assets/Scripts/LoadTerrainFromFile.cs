﻿ using System;
 using UnityEditor;
 using UnityEngine;
 public class LoadTerrainFromFile: MonoBehaviour
 {
     [SerializeField] Texture2D heightmap;
     
     [SerializeField] float heightFactor = 0.5f;
     [SerializeField] float heightOffset = 0.0f;
     [SerializeField] bool flip = false;
     private TerrainData _terrainData;
     private int _terrainWidth;
     private int _terrainHeight;
     private int _heightmapWidth;
     private int _heightmapHeight;
     private float[,] _heightmapData;

     Color[] outputColors;
     
     // method to resize Texture for terrain and scale to have smooth edges 
     private void resizeTexture(ref Color[] coloursArr, Color[] resizeMapColors)
     {
         var ratioX = (float)_heightmapWidth / _terrainWidth;
         var ratioY = (float)_heightmapHeight / _terrainWidth;
         for (var x = 0; x < _terrainWidth; x++) {
             var scaleX = x * _terrainWidth;
             var pY = Mathf.FloorToInt(ratioY * x) * _heightmapWidth;
            for (var y = 0; y < _terrainWidth; y++) {
                 coloursArr[scaleX + y] = resizeMapColors[Mathf.FloorToInt(pY + ratioX * y)];
             }
         }
     }
      void Start()
     {
         if (heightmap == null) {
         #if UNITY_EDITOR
             EditorUtility.DisplayDialog("Texture was not selected!", "Select a correct texture", "Ok");
        #else
        
        #endif  
             enabled = false;
             return;
         }
         if (!heightmap.isReadable) {
         #if UNITY_EDITOR
             EditorUtility.DisplayDialog("Texture is not readable", "Change asset import settings", "Ok");
        #else 
        
        #endif
             enabled = false;
             return;
         }
         _terrainData = Terrain.activeTerrain.terrainData;
         _terrainWidth = _terrainData.heightmapResolution;
         _terrainHeight = _terrainData.heightmapResolution;
         _heightmapWidth = heightmap.width;
         _heightmapHeight = heightmap.height;
         _heightmapData = _terrainData.GetHeights(0, 0, _terrainWidth, _terrainHeight);
         var resizeMapColors = heightmap.GetPixels();
         outputColors = new Color[_terrainWidth * _terrainHeight];
         
         if (_terrainWidth != _heightmapWidth || _heightmapHeight != _heightmapWidth) {
            // If the size of the heightmap and terrain is different resize texture using nearest-neighbor scaling
            resizeTexture(ref outputColors, resizeMapColors);
         }
         else {
             // if heightmap is the same size of the terrain
             outputColors = resizeMapColors;
         }
         
         // set heights of the terrain for appropriate colors
         for (var x = 0; x < _terrainHeight; x++) {
             for (var y = 0; y < _terrainWidth; y++) {
                 float flipF = flip ? -1.0f : 1.0f;
                 // 2 dimensional array to one dimensional array
                 _heightmapData[y, x] = (flipF * outputColors[y * _terrainHeight + x].grayscale) * heightFactor + heightOffset;
             }
         }
         _terrainData.SetHeights(0, 0, _heightmapData);
     }

 }