﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerChecker : MonoBehaviour
{

   void OnTriggerEnter(Collider other) {
      if (other.CompareTag(GameManager.playerTag)) {
         //Debug.Log("enter. Applying force");
         other.GetComponent<Rigidbody>().AddForce(5000 * transform.forward);
      }
   }

   void OnTriggerStay(Collider other) {
      if (other.CompareTag(GameManager.playerTag)) {
         //Debug.Log("stay");
         other.GetComponent<Rigidbody>().AddForce(500 * transform.forward);
      }
   }

   void OnTriggerExit(Collider other) {
      if (other.CompareTag(GameManager.playerTag)) {
         //Debug.Log("exit");
      }
   }

}
