﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FlashlightSwitcher : MonoBehaviour {

    [SerializeField] Light sun;
    [SerializeField] Light flashlight;

    [SerializeField] Material skyboxDay;
    [SerializeField] Material skyboxNight;

    void Update() {
        if (Input.GetKeyUp(KeyCode.F)) {
            sun.enabled = !sun.enabled;
            flashlight.enabled = !sun.enabled;

            if (sun.enabled) {
                RenderSettings.skybox = skyboxDay;
            }
            else {
                RenderSettings.skybox = skyboxNight;
            }
        }
    }

}
