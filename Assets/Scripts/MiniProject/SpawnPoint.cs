﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour {

    GameObject player;
    
    void Start() {
        player = GameManager.instance.GetPlayer();
        Spawn();
    }

    void Spawn() {
        player.transform.position = transform.position;
    }

    public void Respawn() {
        Spawn();
        GameManager.instance.AddPenalty();
    }

}
