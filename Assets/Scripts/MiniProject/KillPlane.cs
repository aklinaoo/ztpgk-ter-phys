﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlane : MonoBehaviour {

    [SerializeField] SpawnPoint spawn;

    void OnTriggerEnter(Collider other) {
        if (other.CompareTag(GameManager.playerTag)) {
           spawn.Respawn();
        }
    }

}
