﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour {

    [SerializeField] TextMeshProUGUI display;
    const string displayText = "Time: {0}\nScore: {1}\nLast time: {2}\nLast score: {3}";
    
    void Update() {
        display.text = string.Format(displayText, 
            GameManager.instance.timeCounter.ToString("F1"), 
            GameManager.instance.pointCounter.ToString(),
            GameManager.instance.LastTime().ToString("F1"), 
            GameManager.instance.LastPoints().ToString());
    }

}
