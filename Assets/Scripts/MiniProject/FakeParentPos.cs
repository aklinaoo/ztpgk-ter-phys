﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeParentPos : MonoBehaviour {

    [SerializeField] Transform fakeParent;
    [SerializeField] Vector3 offset;

    void LateUpdate() {
        transform.position = fakeParent.position + (Quaternion.LookRotation(fakeParent.forward) * offset);
    }

}
