﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public const string playerTag = "Player";

    public GameObject GetPlayer() => player;
    [SerializeField] GameObject player;

    [SerializeField] float penaltySize = 5.0f;
    
    public int pointCounter { get; private set; }
    public float timeCounter { get; private set; }

    const string keyPoints = "pointCounterVal";
    const string keyTime = "timeCounterVal";

    public float LastTime() => PlayerPrefs.GetFloat(keyTime);
    public int LastPoints() => PlayerPrefs.GetInt(keyPoints);

    void Start() {
        timeCounter = 0;
    }

    void Update() {
        timeCounter += Time.deltaTime;
        if (Input.GetKeyUp(KeyCode.R)) {
            ResetScene();
        }
    }

    public void SaveScore() {
        PlayerPrefs.SetInt(keyPoints, pointCounter);
        PlayerPrefs.SetFloat(keyTime, timeCounter);
        PlayerPrefs.Save();
    }

    public void ResetScene() {

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public static GameManager instance {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<GameManager>();
            }
            return _instance;
        }
    }
    static GameManager _instance;


    public void AddPenalty() {
        timeCounter += penaltySize;
    }

    public void AddPoint() {
        pointCounter++;
    }

}
