﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame : MonoBehaviour  {
    
    void OnTriggerEnter(Collider col)  {
        if (col.CompareTag(GameManager.playerTag))  {
            GameManager.instance.SaveScore();
            GameManager.instance.ResetScene();
        }
    }
}
