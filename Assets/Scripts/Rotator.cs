﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    [SerializeField] float x = 50;
    [SerializeField] float y = 60;
    [SerializeField] float z = 40;

    [SerializeField] Space space = Space.Self;

    void Update()
    {
        transform.Rotate(x * Time.deltaTime, y * Time.deltaTime, z * Time.deltaTime, space);    
    }
}
