﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceAdder : MonoBehaviour {

    [SerializeField] Rigidbody rb;
    float torqueSpeed = 50;
    float forceStrength = 500;
    
    void FixedUpdate() {
        if (Input.GetKeyUp(KeyCode.Space)) {
            rb.AddForce(forceStrength * transform.right);
        }
        
        float h = Input.GetAxis("Horizontal") * torqueSpeed * Time.deltaTime;
        float v = Input.GetAxis("Vertical") * torqueSpeed * Time.deltaTime;
        
        rb.AddTorque(transform.up * h);
        rb.AddTorque(transform.forward * v);
    }

}
